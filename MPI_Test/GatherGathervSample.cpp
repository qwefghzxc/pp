// clang-format off
#include <mpi.h>
// clang-format on
#include <cstdio>
#define WORLD MPI_COMM_WORLD

using namespace std;


// ! 所有 rank 都發送「相同型別且相同數量」的資料給 root
/**
 *  int MPI_Gather (
 *      const void *    sendbuf,    // ? 要發送的資料
 *      int             sendcount,  // ? 要發送的資料數量
 *      MPI_Datatype    sendtype,   // ? 發送的資料型別
 *      void *          recvbuf,    // ! root 要將資料存到哪（非 root 可傳 NULL）
 *      int             recvcount,  // ! root 要接收的資料數量
 *      MPI_Datatype    recvtype,   // ! root 要接收的資料型別
 *      int             root,       // ? 發送到哪個 rank
 *      MPI_Comm        comm        // ? 透過哪個 COMM 通訊
 *  )
 **/

// ! 所有 rank 都發送「相同型別」但「任意數量」的資料給 root
/**
 *  int MPI_Gatherv (
 *      const void*     sendbuf,    // ? 傳送的資料存在哪
 *      int             sendcount,  // ? 傳幾筆資料給 root（應跟 recvcounts 相符）
 *      MPI_Datatype    sendtype,   // ? 傳送的資料型別
 *      void*           recvbuf,    // ! root 接收的資料要存在哪裡
 *      const int*      recvcounts, // ! root 接收的資料依序各有幾個
 *      const int*      displs,     // ! root 接收的資料依序要從哪個 index 開始放
 *      MPI_Datatype    recvtype,   // ! root 要接收的資料型別
 *      int             root,       // ? 要發送到哪個 rank
 *      MPI_Comm        comm        // ? 透過哪個 COMM 通訊
 *  )
 **/

void Gather2DArray ( int rank, int size, int d1Size ) {
    int result[size][d1Size];

    // if root, clear result array
    if ( rank == 0 ) {
        for ( int i = 0; i < size; ++i )
            for ( int j = 0; j < d1Size; ++j )
                result[i][j] = -1;
    }

    // all rank init array
    int localArray[d1Size];
    for ( int i = 0; i < d1Size; ++i )
        localArray[i] = rank + i * i;

    // gather local array to root
    MPI_Gather ( localArray, d1Size, MPI_INT, result, d1Size, MPI_INT, 0, WORLD );

    // root show result
    if ( rank == 0 ) {
        // print result
        printf ( "gather result : \n" );
        for ( int i = 0; i < size; ++i ) {
            for ( int j = 0; j < d1Size; ++j )
                printf ( "%d\t", result[i][j] );
            printf ( "\n" );
        }
    }
}

void Gather1DArray ( int rank, int size ) {
    int rSquare = rank * rank;
    // 每個 rank 傳 rank^2 個資料
    int recvCounts[size];
    for ( int i = 0; i < size; ++i )
        recvCounts[i] = i * i;

    // 從 sum( (rank-1)^2 ) 開始放
    int displacements[size];
    for ( int i = 0; i < size; ++i )
        displacements[i] = ( i - 1 ) * i * ( 2 * i - 1 ) / 6;

    // 各自產生 rank^2 個 data
    int data[rSquare];
    for ( int i = 0; i < rSquare; ++i )
        data[i] = rSquare + i;

    // gather 所有 data 到 root
    int total = ( size - 1 ) * size * ( 2 * size - 1 ) / 6;
    int result[total];

    if ( rank == 0 )
        for ( int i = 0; i < total; ++i )
            result[i] = -99;

    MPI_Gatherv ( data, rSquare, MPI_INT, result, recvCounts, displacements, MPI_INT, 0, WORLD );

    // root print gather result
    if ( rank == 0 ) {
        for ( int i = 0; i < size; ++i ) {
            for ( int j = 0; j < recvCounts[i]; ++j )
                printf ( "%d\t", result[displacements[i] + j] );
            printf ( "\n" );
        }
    }
}

int main ( int argc, char const *argv[] ) {

    MPI_Init ( NULL, NULL );
    int rank, size;
    MPI_Comm_size ( WORLD, &size );
    MPI_Comm_rank ( WORLD, &rank );

    Gather2DArray ( rank, size, 7 );
    Gather1DArray ( rank, size );
    MPI_Finalize ( );
    return 0;
}