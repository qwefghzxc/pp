// clang-format off
#include <mpi.h>
// clang-format on
#include <cstdio>
#include <cstddef>
#define WORLD MPI_COMM_WORLD

using namespace std;

typedef struct SAMPLE {
    int v1;
    double v2;
    int v3;
    SAMPLE ( ) : v1 ( -1 ), v2 ( -1 ), v3 ( -1 ) {}
    SAMPLE ( int vi1, double vd, int vi2 ) : v1 ( vi1 ), v2 ( vd ), v3 ( vi2 ) {}
} Sample;

// !
/**
 *  int MPI_Type_struct (
 *      int                 count,                  // ? 有幾個 type
 *      const int *         array_of_blocklengths,  // ? 各個 type 依序的數量
 *      const MPI_Aint *    array_of_displacements, // ? 各個 type 與 element[0] 的距離
 *      const MPI_Datatype* array_of_types,         // ? 依序有哪些 type
 *      MPI_Datatype *      newtype                 // ! 存成什麼 struct
 *  )
 **/

// ! 獲得某個 mpi_type 的記憶體大小
/**
 *  int MPI_Type_extent (
 *      MPI_Datatype    datatype,   // ? 什麼 type
 *      MPI_Aint *      extent      // ! 這個 type 的記憶體大小要存到哪
 *  )
 **/

void Scatter1DStructArray ( int rank, int size ) {
    Sample values[size];

    if ( rank == 0 ) {
        for ( int i = 0; i < size; ++i )
            values[i] = SAMPLE ( 100 * i + i, 200 * i + i, -100 * i - i );
    }

    // regist my struct
    MPI_Datatype MyType;
    int blockLengths[3] = {1, 1, 1};
    MPI_Aint displacements[3] = {
        offsetof ( Sample, v1 ), offsetof ( Sample, v2 ), offsetof ( Sample, v3 )};
    MPI_Datatype types[3] = {MPI_INT, MPI_DOUBLE, MPI_INT};
    MPI_Type_struct ( 3, blockLengths, displacements, types, &MyType );
    MPI_Type_commit ( &MyType );

    // scatter to all ranks
    Sample result;
    MPI_Scatter ( values, 1, MyType, &result, 1, MyType, 0, WORLD );

    // print result
    printf ( "Rank %d Values : v1(%d), v2(%lf), v3(%d) \n", rank, result.v1, result.v2, result.v3 );
}

int main ( int argc, char const *argv[] ) {

    MPI_Init ( NULL, NULL );
    int rank, size;
    MPI_Comm_size ( WORLD, &size );
    MPI_Comm_rank ( WORLD, &rank );

    Scatter1DStructArray ( rank, size );

    MPI_Finalize ( );
    return 0;
}