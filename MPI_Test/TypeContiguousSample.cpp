// clang-format off
#include <mpi.h>
// clang-format on
#include <cstdio>
#define WORLD MPI_COMM_WORLD

using namespace std;

// ! 將連續 n 個相同 mpi_type 的 data 宣告成另外一個 type
/**
 * int MPI_Type_contiguous (
 *      int             count,      // ? 連續幾個相同的 type
 *      MPI_Datatype    oldtype,    // ? 什麼 type
 *      MPI_Datatype *  newtype     // ! 新的 type
 *  )
 **/



void ScatterEveryRow ( int rank, int size, int d1size ) {
    MPI_Datatype typeRow;
    MPI_Type_contiguous ( d1size, MPI_INT, &typeRow );
    MPI_Type_commit ( &typeRow );

    int values[size][d1size];

    // root init values
    if ( rank == 0 ) {
        for ( int i = 0; i < size; ++i ) {
            for ( int j = 0; j < d1size; ++j )
                values[i][j] = 100 * i + j;
        }
    }

    int result[d1size];
    for ( int i = 0; i < d1size; ++i )
        result[i] = -99;

    MPI_Scatter ( values, 1, typeRow, result, 1, typeRow, 0, WORLD );

    // print recv result
    char msg[200];
    char *start = msg;
    for ( int i = 0; i < d1size; ++i )
        start += sprintf ( start, "%d\t  ", result[i] );
    printf ( "Rank %d, values is : %s\n", rank, msg );
}

int main ( int argc, char const *argv[] ) {

    MPI_Init ( NULL, NULL );
    int rank, size;
    MPI_Comm_size ( WORLD, &size );
    MPI_Comm_rank ( WORLD, &rank );

    ScatterEveryRow ( rank, size, 5 );

    MPI_Finalize ( );
    return 0;
}