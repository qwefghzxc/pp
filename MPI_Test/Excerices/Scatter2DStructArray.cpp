// clang-format off
#include <mpi.h>
// clang-format on
#include <cstdio>
#include <cstddef>
#define WORLD MPI_COMM_WORLD

using namespace std;

typedef struct RGBTRIPLE {
    char R, G, B;
    RGBTRIPLE ( ) : R ( 0 ), G ( 0 ), B ( 0 ) {}
    RGBTRIPLE ( char r, char g, char b ) : R ( r ), G ( g ), B ( b ) {}
} RGB;

void BuildRGBRow ( int rowSize, MPI_Datatype *structRGB, MPI_Datatype *typeRow ) {
    // build RGB strcut
    int blockCounts[] = {1, 1, 1};
    MPI_Aint displacements[] = {offsetof ( RGB, R ), offsetof ( RGB, G ), offsetof ( RGB, B )};
    MPI_Datatype types[] = {MPI_CHAR, MPI_CHAR, MPI_CHAR};
    MPI_Type_struct ( 3, blockCounts, displacements, types, structRGB );
    MPI_Type_commit ( structRGB );

    // build RGB Row strcut
    MPI_Type_contiguous ( rowSize, *structRGB, typeRow );
    MPI_Type_commit ( typeRow );
}

void Scatter2DStructArray ( int rank, int size, int d1size ) {
    // build row and rgb struct
    MPI_Datatype structRGB, typeRow;
    BuildRGBRow ( d1size, &structRGB, &typeRow );

    // root init 2d struct array
    RGB values[size][d1size];

    if ( rank == 0 ) {
        for ( int i = 0; i < size; ++i ) {
            for ( int j = 0; j < d1size; ++j )
                values[i][j] = RGBTRIPLE ( i * 3 + j, i * 3 + j + 1, i * 3 + j + 2 );
        }
        printf ( "Root Values is : \n" );
        for ( int i = 0; i < size; ++i ) {
            for ( int j = 0; j < d1size; ++j )
                printf ( "(%2x,%2x,%2x), ", values[i][j].R, values[i][j].G, values[i][j].B );
            printf ( "\n" );
        }
    }

    MPI_Barrier ( WORLD );

    // scatter 2d array
    RGB result[d1size];
    MPI_Scatter ( values, 1, typeRow, result, 1, typeRow, 0, WORLD );

    // all ranks print result
    char msg[500], *start = msg;
    for ( int i = 0; i < d1size; ++i )
        start += sprintf ( start, "(%2x,%2x,%2x), ", result[i].R, result[i].G, result[i].B );
    printf ( "rank %d : %s\n", rank, msg );
}

int main ( int argc, char const *argv[] ) {

    MPI_Init ( NULL, NULL );
    int rank, size;
    MPI_Comm_size ( WORLD, &size );
    MPI_Comm_rank ( WORLD, &rank );

    Scatter2DStructArray ( rank, size, 7 );

    MPI_Finalize ( );
    return 0;
}