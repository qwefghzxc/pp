// clang-format off
#include <mpi.h>
// clang-format on
#include <cstdio>

using namespace std;

typedef struct MYTYPE {
    int v1;
    double v2;
    int v3;
    MYTYPE ( int vi, double vd, int vs ) : v1 ( vi ), v2 ( vd ), v3 ( vs ) {}

} MyType;

// ! 將複數個變數（可以不同 type）組成「struct」
/**
 *  int MPI_Type_create_struct (
 *       int            count,              // ? strcut 的 data size
 *       int            blocklengths[],     // ? 每個 type 的 data 數
 *       MPI_Aint       displacements[],    // ? struct 中每個 element 與 element[0] 之間的「距離」
 *       MPI_Datatype   types[],            // ? struct 是「依序」由哪些 MPI_Datatype 組成的
 *       MPI_Datatype*  newType             // ! 建立的 strcut 資訊回傳的位置
 *  )
 **/

// ! 將某物件的 addr 資訊存到 pAddress 中，用來找出「與 element[0]」的距離、建立 displaments[]
/**
 *  int MPI_Get_address (
 *      void*       pLocation,  // ? 目標物件的 address
 *      MPI_Aint*   pAddress    // ! 物件的記憶體位置要儲存到哪
 *  )
 **/


// ! 建立自己的 mpi datatype
void BuildMyType ( int *v1_int, double *v2_double, int *v2_int, MPI_Datatype *newType ) {
    // 三個 tpye 都只有一個 data（block）
    int blocklengths[] = {1, 1, 1};

    // 依序告知有哪些 type
    MPI_Datatype types[] = {MPI_INTEGER, MPI_DOUBLE, MPI_INTEGER};

    // 獲取三個 arg obj 的 address 位置
    MPI_Aint pV1, pV2, pV3;
    MPI_Get_address ( v1_int, &pV1 );
    MPI_Get_address ( v2_double, &pV2 );
    MPI_Get_address ( v2_int, &pV3 );

    // 根據 address 位置決定 type address 位移量
    MPI_Aint displacements[] = {0, pV2 - pV1, pV3 - pV1};

    // ! 根據以上資訊正式建立 struct 並存到 *newType 中
    MPI_Type_create_struct ( 3, blocklengths, displacements, types, newType );
    // ! 必須 commit MPI 才會接受此 strcut
    MPI_Type_commit ( newType );
}

void Get_input ( int rank, int total, int *v1, double *v2, int *v3 ) {
    MPI_Datatype myType;
    BuildMyType ( v1, v2, v3, &myType );

    printf ( "[%d] before bcast : %d, %lf, %d \n", rank, *v1, *v2, *v3 );
    if ( rank == 0 ) {
        printf ( "scanf values : int, double, int :" );
        scanf ( "%d %lf %d", v1, v2, v3 );
    }
    // ! 透過 element[0] 以及 displacement 等資訊定位各個 element 的記憶體位置
    MPI_Bcast ( v1, 1, myType, 0, MPI_COMM_WORLD );
    MPI_Barrier ( MPI_COMM_WORLD );
    printf ( "[%d] after bcast : %d, %lf, %d \n", rank, *v1, *v2, *v3 );
}

int main ( int argc, char const *argv[] ) {
    int rank, total;
    MPI_Init ( NULL, NULL );
    MPI_Comm_size ( MPI_COMM_WORLD, &total );
    MPI_Comm_rank ( MPI_COMM_WORLD, &rank );

    MyType test ( 999, 888, 777 );
    Get_input ( rank, total, &test.v1, &test.v2, &test.v3 );

    MPI_Finalize ( );
    return 0;
}
