// clang-format off
#include <mpi.h>
// clang-format on
#include <cstdio>
#include <cstdlib>

using namespace std;
#define WORLD MPI_COMM_WORLD

// ! 將資料分送到所有
/**
 * int MPI_Scatter (
 *      const void *    sendbuf,    // ? 要發送的資料起始位置
 *      int             sendcount,  // ? 要發送幾筆資料
 *      MPI_Datatype    sendtype,   // ? 傳發的資料型別
 *      void *          recvbuf,    // ! 接收的資料儲存到哪
 *      int             recvcount,  // ! 接收幾筆資料
 *      MPI_Datatype    recvtype,   // ! 接收資料的型別
 *      int             root,       // ? 由哪個 rank 發送
 *      MPI_Comm        comm        // ? 發送到此 COMM 上所有的 rank
 *  )
 **/

// !
/**
 *  MPI_Scatterv (
 *      const void*     sendBuf,            // ? 要傳送的資料存在哪
 *      const int       sendCounts[],       // ? 要傳的資料依序各有幾筆
 *      const int       displacements[],    // ? 依序要從哪個 index 開始傳資料
 *      MPI_Datatype    sendType,           // ? 傳送的資料型別
 *      void *          recvBuf,            // ! 接收的資料要存在哪
 *      MPI_Datatype    recvType,           // ! 接收的資料型別
 *      int             root,               // ? 哪個 rank 負責傳資料
 *      MPI_Comm        comm                // ? 分送到這個 Comm 的所有 rank
 *  )
 **/

void Scatter2DArray ( int rank, int size, int d1size ) {
    int *values[size];
    // root init values
    if ( rank == 0 ) {
        for ( int i = 0; i < size; ++i ) {
            values[i] = new int[d1size];
            for ( int j = 0; j < d1size; ++j )
                values[i][j] = i * i + j;
        }
    }

    // scatter to all rank
    int result[d1size];
    MPI_Scatter ( values, d1size, MPI_INT, result, d1size, MPI_INT, 0, WORLD );

    MPI_Barrier ( WORLD );
    if ( rank != 0 ) {
        printf ( "rank [%d] get : ", rank );
        for ( int i = 0; i < d1size; ++i )
            printf ( "%d\t", result[i] );
        printf ( "\n" );
    }
}

void Scatter1DArray ( int rank, int size ) {
    int *values[size];

    // root init values
    if ( rank == 0 ) {
        for ( int i = 0; i < size; ++i ) {
            values[i] = new int[i * i];
            for ( int j = 0; j < i * i; ++j )
                values[i][j] = 100 * i + j;
        }
    }

    // sendCounts and displacements
    int sendCounts[size];
    int displacements[size];
    for ( int i = 0; i < size; ++i ) {
        sendCounts[i] = i * i;
        if ( i == 0 )
            displacements[i] = 0;
        else
            displacements[i] = displacements[i - 1] + sendCounts[i - 1];
    }

    // scatterv
    int total = rank * rank;
    int result[total];
    MPI_Scatterv ( values, sendCounts, displacements, MPI_INT, result, total, MPI_INT, 0, WORLD );

    // print results
    MPI_Barrier ( WORLD );
    if ( rank != 0 ) {
        printf ( "rank [%d] get : ", rank );
        for ( int i = 0; i < total; ++i )
            printf ( "%d\t", result[i] );
        printf ( "\n" );
    }
}


int main ( int argc, char const *argv[] ) {

    MPI_Init ( NULL, NULL );
    int rank, size;
    MPI_Comm_size ( WORLD, &size );
    MPI_Comm_rank ( WORLD, &rank );

    Scatter2DArray ( rank, size, 6 );
    MPI_Barrier ( WORLD );
    Scatter1DArray ( rank, size );

    MPI_Finalize ( );
    return 0;
}