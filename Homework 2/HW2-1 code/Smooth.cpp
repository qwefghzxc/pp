// clang-format off
#include <mpi.h>
// clang-format on

#include <cstdio>
#include <cstdlib>
#include <cstddef>

#include <fstream>
#include <iostream>

#include <string>
#include <cstring>

#include "bmp.h"

using namespace std;

//定義平滑運算的次數
#define NSmooth 1000
#define COMM MPI_COMM_WORLD

/*******************************************/
/*變數宣告：                                 */
/*  bmpHeader    ： BMP檔的標頭              */
/*  bmpInfo      ： BMP檔的資訊              */
/*  **BMPSaveData： 儲存要被寫入的像素資料     */
/*  **BMPData    ： 暫時儲存要被寫入的像素資料  */
/*******************************************/
BMPHEADER bmpHeader;
BMPINFO bmpInfo;
RGBTRIPLE **BMPSaveData = NULL;
RGBTRIPLE **BMPData = NULL;

// ! 自訂的 mpi 資料型別（RGB struct 以及 RGB Row）
MPI_Datatype mpiRGB, mpiRow;

// ! 讀取以及儲存 bmp 檔案
int readBMP ( char *fileName );
int saveBMP ( char *fileName );

// ! 交換資料依據以及儲存位置的指標
void swap ( RGBTRIPLE *a, RGBTRIPLE *b );
RGBTRIPLE **alloc_memory ( int Y, int X );

// ! 自訂 mpi_type
void DefineRGBRow ( LONG rowSize, MPI_Datatype *mpiRGB, MPI_Datatype *mpiRow );

// ! 根據 size 計算出 resps, disps, heads, tails 等平行化處理需要的資訊
void ParalellingInfos ( int size, int resps[], int disps[], int heads[], int tails[] );

int main ( int argc, char *argv[] ) {
    char infileName[] = "input.bmp";
    char outfileName[] = "output2.bmp";
    double startwtime, endwtime;

    // mpi init
    int size, rank;
    MPI_Status status;
    MPI_Init ( NULL, NULL );
    MPI_Comm_size ( COMM, &size );
    MPI_Comm_rank ( COMM, &rank );

    // 所有 rank 都讀取檔案, 並將資訊存在 bmpHeader 以及 BMPSaveData
    if ( readBMP ( infileName ) )
        cout << "[" << rank << "] Read file success!!" << endl;
    else
        cout << "[" << rank << "] Read file fails!!" << endl;

    // 從讀取 bmp 完畢後開始計算時間
    if ( rank == 0 )
        startwtime = MPI_Wtime ( );

    // 根據圖片長寬分配記憶體給 BMPData 作為本次平滑化的依據資料的儲存空間
    // 並在稍後與 BMPSaveData 交換指標（BMPSaveData 在 readBMP() 時分配, bmpInfo 則為 BMP 資訊)
    BMPData = alloc_memory ( bmpInfo.biHeight, bmpInfo.biWidth );

    // ! 根據圖片大小將每個 RGB struct 以及每個 RGB row 定義成 MPI type
    DefineRGBRow ( bmpInfo.biWidth, &mpiRGB, &mpiRow );

    // ! 根據 comm 的 size 決定各個 rank 負責的 rows 以及相關資訊
    int resps[size];        // ? 各個 rank 負責的 row 數
    int disps[size];        // ? 各個 rank 負責的起始 row index
    int heads[size];        // ? 各個 rank 首個 row 所需的 row（上一個 block 的最後一個 row）
    int tails[size];        // ? 各個 rank 最後一個 row 所需的 row（下一個 block 的第一個 row）
    ParalellingInfos ( size, resps, disps, heads, tails );

    // ! 找出自己的上、下一個 rank 編號以及 head, tail 位置
    int prevRank = ( rank == 0 ) ? size - 1 : rank - 1;
    int nextRank = ( rank == ( size - 1 ) ) ? 0 : rank + 1;

    // clang-format off
    // if ( rank == 0 ) {
    //     for ( int i = 0; i < size; ++i )
    //         printf ( "[%d] %4d rows, from %4d, needs:(%4d,%4d)\n", i, resps[i], disps[i], heads[i], tails[i] );
    // }
    // clang-format on

    MPI_Barrier ( COMM );
    // 根據 define 進行 NSmooth 次平滑化處理
    for ( int count = 0; count < NSmooth; count++ ) {
        // ! 每一輪開始前需要將 上次結果、上次資料 兩個指標交換
        swap ( BMPData, BMPSaveData );

        // ! 如果有多台機器( size > 1 )，更新邊界資訊(headRow、tailRow)
        if ( size > 1 ) {
            // 送出自己的第一個 row 「給上一個 rank」，並從「下一個 rank」接收 tail
            MPI_Send ( BMPData[disps[rank]], 1, mpiRow, prevRank, 0, COMM );
            MPI_Recv ( BMPData[tails[rank]], 1, mpiRow, nextRank, 0, COMM, &status );
            MPI_Barrier ( COMM );
            // 送出自己的最後一個 row 「給下一個 rank」，並從「上一個 rank」接收 head
            MPI_Send ( BMPData[disps[rank] + resps[rank] - 1], 1, mpiRow, nextRank, 1, COMM );
            MPI_Recv ( BMPData[heads[rank]], 1, mpiRow, prevRank, 1, COMM, &status );
            MPI_Barrier ( COMM );
        }

        // ! 平滑化一次負責的範圍
        for ( int i = disps[rank]; i < disps[rank] + resps[rank]; ++i ) {
            for ( int j = 0; j < bmpInfo.biWidth; ++j ) {
                // 上下左右的像素 index
                int Top = ( i == 0 ) ? bmpInfo.biHeight - 1 : i - 1;
                int Down = ( i == ( bmpInfo.biHeight - 1 ) ) ? 0 : i + 1;
                int Left = ( j == 0 ) ? bmpInfo.biWidth - 1 : j - 1;
                int Right = ( j == ( bmpInfo.biWidth - 1 ) ) ? 0 : j + 1;

                // 與上下左右像素做平均，並四捨五入
                BMPSaveData[i][j].rgbBlue =
                    ( double ) ( BMPData[i][j].rgbBlue + BMPData[Top][j].rgbBlue +
                                 BMPData[Down][j].rgbBlue + BMPData[i][Left].rgbBlue +
                                 BMPData[i][Right].rgbBlue ) /
                        5 +
                    0.5;
                BMPSaveData[i][j].rgbGreen =
                    ( double ) ( BMPData[i][j].rgbGreen + BMPData[Top][j].rgbGreen +
                                 BMPData[Down][j].rgbGreen + BMPData[i][Left].rgbGreen +
                                 BMPData[i][Right].rgbGreen ) /
                        5 +
                    0.5;
                BMPSaveData[i][j].rgbRed =
                    ( double ) ( BMPData[i][j].rgbRed + BMPData[Top][j].rgbRed +
                                 BMPData[Down][j].rgbRed + BMPData[i][Left].rgbRed +
                                 BMPData[i][Right].rgbRed ) /
                        5 +
                    0.5;
            }
        }
        MPI_Barrier ( COMM );
    }

    // clang-format off
    if ( size > 1) {
        // ! 完成 NSmooth 次平滑化後，root Gatherv 所有 rank 的結果，存到 BMPData
        MPI_Gatherv ( BMPSaveData[disps[rank]], resps[rank], mpiRow, BMPData[0], resps, disps, mpiRow, 0, COMM );
        // ! saveBMP 是以 BMPSaveData 輸出，所以要再把 BMPData swap 一次
        // ! 若是 size == 1 「且沒有 Gatherv」則不可再 swap 
        swap ( BMPData, BMPSaveData );
    }
    // clang-format on

    // root 獲得結束時間、執行時間
    MPI_Barrier ( COMM );
    if ( rank == 0 ) {
        // 完成 NSmooth次平滑化處理後, 輸出成 bmp 圖檔 (此時最新的平滑化資訊存在 BMPSaveData 中)
        if ( saveBMP ( outfileName ) )
            cout << "Save file sucess!!" << endl;
        else
            cout << "Save file fails!!" << endl;

        endwtime = MPI_Wtime ( );
        cout << "The execution time = " << endwtime - startwtime << endl;
    }

    MPI_Finalize ( );

    // 釋放動態分配的空間
    free ( BMPData[0] );
    free ( BMPSaveData[0] );
    free ( BMPData );
    free ( BMPSaveData );

    return 0;
}

// ! 根據資訊將 RGB 以及 Row 定義成 mpi type
void DefineRGBRow ( LONG rowSize, MPI_Datatype *mpiRGB, MPI_Datatype *mpiRow ) {
    // build RGB strcut
    int blockCounts[] = {1, 1, 1};
    MPI_Aint displacements[] = {offsetof ( RGBTRIPLE, rgbBlue ),
                                offsetof ( RGBTRIPLE, rgbGreen ),
                                offsetof ( RGBTRIPLE, rgbRed )};

    MPI_Datatype types[] = {MPI_UNSIGNED_CHAR, MPI_UNSIGNED_CHAR, MPI_UNSIGNED_CHAR};
    MPI_Type_struct ( 3, blockCounts, displacements, types, mpiRGB );
    MPI_Type_commit ( mpiRGB );

    // build RGB Row strcut
    MPI_Type_contiguous ( rowSize, *mpiRGB, mpiRow );
    MPI_Type_commit ( mpiRow );
}

// ! 根據 size 計算出 resps, disps, heads, tails 等平行化處理需要的資訊
void ParalellingInfos ( int size, int resps[], int disps[], int heads[], int tails[] ) {

    // * 平均 rows 給所有 rank，多個給最後一個 rank 負責
    int avg = bmpInfo.biHeight / size;
    int remain = bmpInfo.biHeight % size;

    for ( int i = 0; i < size; ++i ) {
        resps[i] = ( i != size - 1 ) ? avg : avg + remain;
        disps[i] = ( i == 0 ) ? 0 : disps[i - 1] + resps[i - 1];
    }

    // * head 為此 rank 的 disp-1, tail 則是下一個 rank 的 disp
    for ( int i = 0; i < size; ++i ) {
        // 第一個 rank 的 head 是圖片的最後一個 row、其餘 rank 則為該 rank 的 disp - 1
        heads[i] = ( i == 0 ) ? bmpInfo.biHeight - 1 : disps[i] - 1;
        // 最後一個 rank 的 tail 是圖片的第一個 row、其餘 rank 則為下一個 rank 的 disp
        tails[i] = ( i == size - 1 ) ? 0 : disps[i + 1];
    }
}


// ! 讀取 bmp 圖檔
int readBMP ( char *fileName ) {
    //建立輸入檔案物件
    ifstream bmpFile ( fileName, ios::in | ios::binary );

    //檔案無法開啟
    if ( !bmpFile ) {
        cout << "It can't open file!!" << endl;
        return 0;
    }

    //如果檔案正確, 讀取BMP圖檔應有的的標頭資料, 並存在 bmpHeader
    bmpFile.read ( ( char * ) &bmpHeader, sizeof ( BMPHEADER ) );

    //判決是否為BMP圖檔
    if ( bmpHeader.bfType != 0x4d42 ) {
        cout << "This file is not .BMP!!" << endl;
        return 0;
    }

    //讀取BMP的資訊, 並存在「bmp Info」中
    bmpFile.read ( ( char * ) &bmpInfo, sizeof ( BMPINFO ) );

    //判斷位元深度是否為24 bits
    if ( bmpInfo.biBitCount != 24 ) {
        cout << "The file is not 24 bits!!" << endl;
        return 0;
    }

    //修正圖片的寬度為4的倍數
    while ( bmpInfo.biWidth % 4 != 0 )
        bmpInfo.biWidth++;

    //動態分配記憶體 ( BMP SaveData 已分配)
    BMPSaveData = alloc_memory ( bmpInfo.biHeight, bmpInfo.biWidth );

    //讀取像素資料
    // for(int i = 0; i < bmpInfo.biHeight; i++)
    //	bmpFile.read( (char* )BMPSaveData[i],
    // bmpInfo.biWidth*sizeof(RGBTRIPLE));
    bmpFile.read ( ( char * ) BMPSaveData[0],
                   bmpInfo.biWidth * sizeof ( RGBTRIPLE ) * bmpInfo.biHeight );

    //關閉檔案
    bmpFile.close ( );

    return 1;
}
// ! 儲存 bmp 圖檔
int saveBMP ( char *fileName ) {
    //判決是否為BMP圖檔
    if ( bmpHeader.bfType != 0x4d42 ) {
        cout << "This file is not .BMP!!" << endl;
        return 0;
    }

    //建立輸出檔案物件
    ofstream newFile ( fileName, ios::out | ios::binary );

    //檔案無法建立
    if ( !newFile ) {
        cout << "The File can't create!!" << endl;
        return 0;
    }

    //寫入BMP圖檔的標頭資料
    newFile.write ( ( char * ) &bmpHeader, sizeof ( BMPHEADER ) );

    //寫入BMP的資訊
    newFile.write ( ( char * ) &bmpInfo, sizeof ( BMPINFO ) );

    //寫入像素資料
    // for( int i = 0; i < bmpInfo.biHeight; i++ )
    //        newFile.write( ( char* )BMPSaveData[i],
    //        bmpInfo.biWidth*sizeof(RGBTRIPLE) );
    newFile.write ( ( char * ) BMPSaveData[0],
                    bmpInfo.biWidth * sizeof ( RGBTRIPLE ) * bmpInfo.biHeight );

    //寫入檔案
    newFile.close ( );

    return 1;
}
// ! 根據長寬分配二維的記憶體空間
RGBTRIPLE **alloc_memory ( int Y, int X ) {
    //建立長度為Y的指標陣列
    RGBTRIPLE **temp = new RGBTRIPLE *[Y];
    RGBTRIPLE *temp2 = new RGBTRIPLE[Y * X];
    memset ( temp, 0, sizeof ( RGBTRIPLE ) * Y );
    memset ( temp2, 0, sizeof ( RGBTRIPLE ) * Y * X );

    //對每個指標陣列裡的指標宣告一個長度為X的陣列
    for ( int i = 0; i < Y; i++ ) {
        temp[i] = &temp2[i * X];
    }

    return temp;
}
// ! 指標交換，用來交換本次平滑化資料依據以及結果
void swap ( RGBTRIPLE *a, RGBTRIPLE *b ) {
    RGBTRIPLE *temp;
    temp = a;
    a = b;
    b = temp;
}
