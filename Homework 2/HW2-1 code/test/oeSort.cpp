// clang-format off
#include "mpi.h"
// clang-format on
#include <cstdio>
#include <cstdlib>
#include <algorithm>

using namespace std;

int GET_Partner(int round, int rank) {
    if (round % 2 == 0)  // 偶數回合
        return (rank % 2 == 0) ? rank + 1 : rank - 1;
    else  // 奇數回合
        return (rank % 2 == 0) ? rank - 1 : rank + 1;
}

int main() {
    MPI_Init(NULL, NULL);

    // 計算時間
    double start_time;

    // cluster 數量以及 rank
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // 只在 main rank 計算時間
    if (rank == 0) start_time = MPI_Wtime();

    int n = -1, *values;
    // main 輸入 n 個數字並且 Bcast 所有數字以及 n
    if (rank == 0) {
        printf("please input 'n' and n numbers to sort.\n");
        scanf("%d", &n);
    }
    // 所有 rank 都藉由 Bcast 得到 n 建立 int* 儲存
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    values = new int[n];

    // main 輸入 n 個數字, 並藉由 Bcast 把 n 個數字都給所有 cluster
    if (rank == 0) {
        for (int i = 0; i < n; ++i) scanf("%d", &values[i]);
    }
    MPI_Bcast(values, n, MPI_INT, 0, MPI_COMM_WORLD);

    // 每組要幾個, 有幾組要多一個
    int quo = n / size;
    int rem = n % size;

    // 開始的 index
    int start = (quo + 1) * (rank < rem ? rank : rem);
    if (rank >= rem) start += (rank - rem) * quo;

    // 每個 rank 負責的數量
    int resps[size];
    for (int i = 0; i < size; ++i) {
        resps[i] = quo + (i < rem ? 1 : 0);
    }
    // 每個 rank 的 start 位置
    int allstart[size];
    for (int i = 0; i < size; ++i) {
        if (i == 0)
            allstart[i] = 0;
        else
            allstart[i] = allstart[i - 1] + resps[i - 1];
    }

    /// 計算開始 -------------------------------------------------

    // sort local
    sort(&values[start], &values[start] + resps[rank]);

    // 進行 size 次交換
    MPI_Status stat;
    for (int round = 0; round < size; ++round) {
        // 找到這一輪的 partner
        int partner = GET_Partner(round, rank);

        // 如果有 partner
        if (partner >= 0 && partner < (size < n ? size : n) && rank < n) {
            // 接收 partner datas 的 buffer
            int bufSize = resps[rank] + resps[partner];
            int buf[bufSize];

            // 偶數先傳送自己的 datas, 然後再接收 parter 的 data
            if (rank % 2 == 0) {
                MPI_Send(&values[start], resps[rank], MPI_INT, partner, partner,
                         MPI_COMM_WORLD);
                MPI_Recv(buf, resps[partner], MPI_INT, partner, rank,
                         MPI_COMM_WORLD, &stat);
            }
            // 奇數則先接收 partner 再傳送自己的 data
            else {
                MPI_Recv(buf, resps[partner], MPI_INT, partner, rank,
                         MPI_COMM_WORLD, &stat);
                MPI_Send(&values[start], resps[rank], MPI_INT, partner, partner,
                         MPI_COMM_WORLD);
            }

            // 把自己的資料也 append 到 buf 中
            for (int i = 0; i < resps[rank]; ++i) {
                buf[resps[partner] + i] = values[start + i];
            }

            // 排序 buffer
            sort(buf, buf + bufSize);

            for (int i = 0; i < resps[rank]; ++i) {
                // rank 大的保留前 resps[rank] 大的 values
                if (rank > partner) {
                    values[start + i] = buf[resps[partner] + i];
                }
                // rank 小的保留後 resps[rank] 小的 values
                else {
                    values[start + i] = buf[i];
                }
            }

            /* DEBUG
            if (rank < partner) {
                printf("ROUND %d | rank(%d,%d) buf values : ", round, rank,
                       partner);
                for (int i = 0; i < bufSize; ++i) printf("%d, ", buf[i]);
                printf("\n");
            }
            */
        }
        // WAIT ALL cluster DONE
        MPI_Barrier(MPI_COMM_WORLD);
    }

    /// 計算結束 -------------------------------------------------

    // main gatherv 結果
    int *results;
    if (rank == 0) {
        results = new int[n];
    }
    MPI_Gatherv(&values[start], resps[rank], MPI_INT, results, resps, allstart,
                MPI_INT, 0, MPI_COMM_WORLD);

    // 在 main 中印出結果並且計算耗費時間
    if (rank == 0) {
        printf("排序結果 ：\n");
        for (int i = 0; i < n; ++i) printf("%d, ", results[i]);
        printf("\n耗費時間 ： %lf\n", MPI_Wtime() - start_time);
    }
    MPI_Finalize();

    return 0;
}
