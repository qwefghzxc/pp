// clang-format off
#include "mpi.h"

// clang-format on
#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
    MPI_Init(NULL, NULL);

    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int datas[5];
    if (rank == 0) {
        for (int i = 0; i < 5; ++i) datas[i] = i * i + i;
    }
    MPI_Bcast(datas, 5, MPI_INT, 0, MPI_COMM_WORLD);

    printf("This is rank %d, get values : ", rank);
    for (int i = 0; i < 5; ++i) printf("%d, ", datas[i]);
    printf("\n");
    MPI_Finalize();

    return 0;
}
