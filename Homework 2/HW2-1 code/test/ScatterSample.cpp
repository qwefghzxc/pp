#include "mpi.h"

#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
    // initialize all clusters
    MPI_Init(NULL, NULL);

    // variables for total and my_id
    int total, my_id;
    MPI_Comm_size(MPI_COMM_WORLD, &total);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_id);

    // make a 2d int array
    int data_set[total][10];
    for (int i = 0; i < total; ++i) {
        for (int j = 0; j < 10; ++j) {
            data_set[i][j] = i * i + j;
        }
    }

    // scatter whole "row"
    int recv_buffer[10];
    MPI_Scatter(data_set, 10, MPI_INT, &recv_buffer, 10, MPI_INT, 0,
                MPI_COMM_WORLD);

    printf("ID[%.2d] get datas : ", my_id);
    for (int i = 0; i < 10; ++i) {
        printf("%d, ", recv_buffer[i]);
    }
    printf("\n");

    // end
    MPI_Finalize();

    return 0;
}
