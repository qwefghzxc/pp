#include "mpi.h"

#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
    MPI_Init(NULL, NULL);

    int total, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &total);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int sendDatas[] = {2, 3, 3, 4, 4};
    int recvIndex[] = {0, 2, 5, 8, 12};

    // make data to send
    int *local_datas = new int[sendDatas[rank]];
    for (int i = 0; i < sendDatas[rank]; ++i) {
        local_datas[i] = rank * rank + i;
    }

    // only root need real buffer
    int *recvBuffer = (rank == 0) ? (new int[16]) : NULL;

    // gather local datas
    MPI_Gatherv(local_datas, sendDatas[rank], MPI_INT, recvBuffer, sendDatas,
                recvIndex, MPI_INT, 0, MPI_COMM_WORLD);

    // main rank printf gatherv result
    if (rank == 0) {
        printf("gather results : ");
        for (int i = 0, j = 0; i < 16; ++i) {
            if (recvIndex[j] == i) {
                printf("\n");
                ++j;
            }
            printf("%d, ", recvBuffer[i]);
        }
    }

    MPI_Finalize();
    return 0;
}
