// clang-format off
#include "mpi.h"
// clang-format on
#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
    MPI_Init(NULL, NULL);

    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int samples[] = {1, 2, 3, 4, 5, 6, 7, 8, 8, 8};
    MPI_Aint disps[10];

    for (int i = 0; i < 10; ++i) {
        if (i == 0)
            disps[0] = 0;
        else
            disps[i] = &samples[i] - &samples[i - 1];
    }

    for (int i = 0; i < 10; ++i) printf("%p, ", *disps[i]);
    printf("\n");

    MPI_Datatype MYTYPE;
    MPI_Type_create_struct(10, samples, disps, MPI_INT, &MYTYPE);

    MPI_Finalize();
    return 0;
}
