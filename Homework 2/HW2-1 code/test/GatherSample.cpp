#include "mpi.h"

#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
    MPI_Init(NULL, NULL);

    int total, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &total);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // every one build a int array with size 10
    int my_data[10];
    for (int i = 0; i < 10; ++i) {
        my_data[i] = rank * rank + i;
    }

    // main rank should gather these data
    if (rank == 0) {
        // main rank should have buffer to save result
        int resBuffer[total][10];
        MPI_Gather(my_data, 10, MPI_INT, resBuffer, 10, MPI_INT, 0,
                   MPI_COMM_WORLD);

        // after recv all datas, printf them
        for (int i = 0; i < total; ++i) {
            printf("%.2d :", i);
            for (int j = 0; j < 10; ++j) {
                printf("%d, ", resBuffer[i][j]);
            }
            printf("\n");
        }
    } else {
        MPI_Gather(my_data, 10, MPI_INT, NULL, 10, MPI_INT, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}
