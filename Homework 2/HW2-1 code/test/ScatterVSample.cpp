#include "mpi.h"

#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
    MPI_Init(NULL, NULL);
    int total, rank;

    MPI_Comm_size(MPI_COMM_WORLD, &total);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // total data size is : 8
    int data[8] = { 1, 2, 3, 4, 5, 6, 7, 8 };

    // use a array to determine send count and recv count
    int sCnts[5] = {2, 2, 2, 1, 1};
    int rCnts[5] = {0, 2, 4, 6, 7};

    // recv buffer size at most 2
    int recvBuffer[2] = {-1, -1};
    MPI_Scatterv(data, sCnts, rCnts, MPI_INT, recvBuffer, sCnts[rank], MPI_INT,
                 0, MPI_COMM_WORLD);

    printf("id [%d] get data : %d, %d\n", rank, recvBuffer[0], recvBuffer[1]);

    MPI_Finalize();
    return 0;
}
