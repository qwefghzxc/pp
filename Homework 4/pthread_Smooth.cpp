#include "bmp.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iostream>
#include <mutex>
#include <pthread.h>
#include <unistd.h>

using namespace std;

//定義平滑運算的次數
#define NSmooth 1000

/*********************************************************/
/*變數宣告：                                             */
/*  bmpHeader    ： BMP檔的標頭                          */
/*  bmpInfo      ： BMP檔的資訊                          */
/*  **BMPSaveData： 儲存要被寫入的像素資料               */
/*  **BMPData    ： 暫時儲存要被寫入的像素資料           */
/*********************************************************/
BMPHEADER bmpHeader;
BMPINFO bmpInfo;
RGBTRIPLE **BMPSaveData = NULL;
RGBTRIPLE **BMPData = NULL;

/*********************************************************/
/*函數宣告：                                             */
/*  readBMP    ： 讀取圖檔，並把像素資料儲存在BMPSaveData*/
/*  saveBMP    ： 寫入圖檔，並把像素資料BMPSaveData寫入  */
/*  swap       ： 交換二個指標                           */
/*  **alloc_memory： 動態分配一個Y * X矩陣               */
/*********************************************************/
int readBMP( char *fileName );        // read file
int saveBMP( char *fileName );        // save file
void swap( RGBTRIPLE *a, RGBTRIPLE *b );
RGBTRIPLE **alloc_memory( int Y, int X );        // allocate memory


/**
 * @brief 等待所有 thread 抵達並完成此 function
 *
 * @param barrier_mutex
 * @param total 總共有幾個 threads
 * @param count 目前已經抵達的 thread 數量（from 0）
 */
void WaitAllThreads( pthread_mutex_t *barrier_mutex, int total, int *count ) {
    pthread_mutex_lock( barrier_mutex );
    ++*count;
    pthread_mutex_unlock( barrier_mutex );
    // busy waiting until all thread arrived
    while ( *count < total )
        usleep( 1 );
    // all thread arrived, reset counter and return
    return;
}
pthread_mutex_t barrierLock;
int firstCounter[NSmooth];
int secondCounter[NSmooth];

/**
 * @brief
 *
 */
typedef struct ThreadArgsStruct {
    int tid, total, initRow, resp;
    ThreadArgsStruct() {}
    ThreadArgsStruct( int id, int size, int row, int resp ) {
        this->tid = id;
        this->total = size;
        this->initRow = row;
        this->resp = resp;
    }
} ThreadArgs;

/**
 * @brief
 *
 *
 * @param args
 * @return void*
 */
void *ParalellSmoothing( void *raw_args ) {
    ThreadArgs* args = (ThreadArgs *)raw_args;
    printf( "th[%d] get %d,%d,%d\n", args->tid, args->total, args->initRow, args->resp );

    for ( int count = 0; count < NSmooth; ++count ) {
        // choose one thread to swap bmpSaveData and bmpData
        if ( args->tid == 0 ) {
            swap( BMPSaveData, BMPData );
        }
        WaitAllThreads( &barrierLock, args->total, &firstCounter[count] );


        // smooth resp rows
        for ( int i = args->initRow; i < ( args->initRow + args->resp ); i++ ) {
            for ( int j = 0; j < bmpInfo.biWidth; j++ ) {
                /*********************************************************/
                /*設定上下左右像素的位置                                 */
                /*********************************************************/
                int Top = i > 0 ? i - 1 : bmpInfo.biHeight - 1;
                int Down = i < bmpInfo.biHeight - 1 ? i + 1 : 0;
                int Left = j > 0 ? j - 1 : bmpInfo.biWidth - 1;
                int Right = j < bmpInfo.biWidth - 1 ? j + 1 : 0;

                /*********************************************************/
                /*與上下左右像素做平均，並四捨五入                       */
                /*********************************************************/
                BMPSaveData[i][j].rgbBlue =
                    (double)( BMPData[i][j].rgbBlue + BMPData[Top][j].rgbBlue + BMPData[Down][j].rgbBlue +
                              BMPData[i][Left].rgbBlue + BMPData[i][Right].rgbBlue ) /
                        5 +
                    0.5;
                BMPSaveData[i][j].rgbGreen =
                    (double)( BMPData[i][j].rgbGreen + BMPData[Top][j].rgbGreen + BMPData[Down][j].rgbGreen +
                              BMPData[i][Left].rgbGreen + BMPData[i][Right].rgbGreen ) /
                        5 +
                    0.5;
                BMPSaveData[i][j].rgbRed =
                    (double)( BMPData[i][j].rgbRed + BMPData[Top][j].rgbRed + BMPData[Down][j].rgbRed +
                              BMPData[i][Left].rgbRed + BMPData[i][Right].rgbRed ) /
                        5 +
                    0.5;
            }
        }
        WaitAllThreads( &barrierLock, args->total, &secondCounter[count] );
    }
    return NULL;
}


int main( int argc, char const *argv[] ) {
    /*********************************************************/
    /*變數宣告：                                             */
    /*  *infileName  ： 讀取檔名                             */
    /*  *outfileName ： 寫入檔名                             */
    /*  startTime    ： 記錄開始時間                         */
    /*  finishTime   ： 記錄結束時間                         */
    /*********************************************************/
    char infileName[] = "input.bmp";
    char outfileName[] = "output.bmp";
    time_t startTime = 0.0, finishTime = 0;

    // 假設 argv[1] 為 thread 的數量
    int total = atoi( argv[1] );

    // 建立相對應數量的 threads
    pthread_t *ths = (pthread_t *)malloc( total * sizeof( pthread_t ) );

    //讀取檔案
    if ( readBMP( infileName ) )
        cout << "Read file successfully!!" << endl;
    else
        cout << "Read file fails!!" << endl;

    // main thread 在讀取完圖片後開始計算時間
    startTime = time( NULL );

    //動態分配記憶體給暫存空間
    BMPData = alloc_memory( bmpInfo.biHeight, bmpInfo.biWidth );

    // 分配各個 thread 要處理的 row
    int avg = bmpInfo.biHeight / total;
    int rem = bmpInfo.biHeight % total;
    int initRow[total];
    int resps[total];

    for ( int tid = 0; tid < total; ++tid ) {
        // resp rows
        resps[tid] = avg + ( tid < rem ? 1 : 0 );
        // start from which row
        if ( tid == 0 )
            initRow[tid] = 0;
        else
            initRow[tid] = initRow[tid - 1] + resps[tid - 1];
    }

    // 每個 thread要傳入不同的 tid, 所以需要建立 array
    ThreadArgs args[total];

    //進行多次的平滑運算
    for ( int tid = 0; tid < total; ++tid ) {
        args[tid] = ThreadArgs( tid, total, initRow[tid], resps[tid] );
        pthread_create( &ths[tid], NULL, ParalellSmoothing, &args[tid] );
    }


    // ! main thread 等待所有 thread 結束
    for ( int i = 0; i < total; ++i )
        pthread_join( ths[i], NULL );

    // 所有 thread 都結束後就清除所有 thread
    free( ths );

    // 輸出圖檔及耗費時間
    if ( saveBMP( outfileName ) )
        cout << "Save file successfully!!" << endl;
    else
        cout << "Save file fails!!" << endl;

    // 紀錄結束時間並印出耗費時間
    finishTime = time( NULL );
    printf( "TIME : %ld Sec.\n", ( finishTime - startTime ) );
}

/*********************************************************/
/* 讀取圖檔                                              */
/*********************************************************/
int readBMP( char *fileName ) {
    //建立輸入檔案物件
    ifstream bmpFile( fileName, ios::in | ios::binary );

    //檔案無法開啟
    if ( !bmpFile ) {
        cout << "It can't open file!!" << endl;
        return 0;
    }

    //讀取BMP圖檔的標頭資料
    bmpFile.read( (char *)&bmpHeader, sizeof( BMPHEADER ) );

    //判決是否為BMP圖檔
    if ( bmpHeader.bfType != 0x4d42 ) {
        cout << "This file is not .BMP!!" << endl;
        return 0;
    }

    //讀取BMP的資訊
    bmpFile.read( (char *)&bmpInfo, sizeof( BMPINFO ) );

    //判斷位元深度是否為24 bits
    if ( bmpInfo.biBitCount != 24 ) {
        cout << "The file is not 24 bits!!" << endl;
        return 0;
    }

    //修正圖片的寬度為4的倍數
    while ( bmpInfo.biWidth % 4 != 0 )
        bmpInfo.biWidth++;

    //動態分配記憶體
    BMPSaveData = alloc_memory( bmpInfo.biHeight, bmpInfo.biWidth );

    //讀取像素資料
    // for(int i = 0; i < bmpInfo.biHeight; i++)
    //	bmpFile.read( (char* )BMPSaveData[i],
    // bmpInfo.biWidth*sizeof(RGBTRIPLE));
    bmpFile.read( (char *)BMPSaveData[0], bmpInfo.biWidth * sizeof( RGBTRIPLE ) * bmpInfo.biHeight );

    //關閉檔案
    bmpFile.close();

    return 1;
}
/*********************************************************/
/* 儲存圖檔                                              */
/*********************************************************/
int saveBMP( char *fileName ) {
    //判決是否為BMP圖檔
    if ( bmpHeader.bfType != 0x4d42 ) {
        cout << "This file is not .BMP!!" << endl;
        return 0;
    }

    //建立輸出檔案物件
    ofstream newFile( fileName, ios::out | ios::binary );

    //檔案無法建立
    if ( !newFile ) {
        cout << "The File can't create!!" << endl;
        return 0;
    }

    //寫入BMP圖檔的標頭資料
    newFile.write( (char *)&bmpHeader, sizeof( BMPHEADER ) );

    //寫入BMP的資訊
    newFile.write( (char *)&bmpInfo, sizeof( BMPINFO ) );

    //寫入像素資料
    // for( int i = 0; i < bmpInfo.biHeight; i++ )
    //        newFile.write( ( char* )BMPSaveData[i],
    //        bmpInfo.biWidth*sizeof(RGBTRIPLE) );
    newFile.write( (char *)BMPSaveData[0], bmpInfo.biWidth * sizeof( RGBTRIPLE ) * bmpInfo.biHeight );

    //寫入檔案
    newFile.close();

    return 1;
}

/*********************************************************/
/* 分配記憶體：回傳為Y*X的矩陣                           */
/*********************************************************/
RGBTRIPLE **alloc_memory( int Y, int X ) {
    //建立長度為Y的指標陣列
    RGBTRIPLE **temp = new RGBTRIPLE *[Y];
    RGBTRIPLE *temp2 = new RGBTRIPLE[Y * X];
    memset( temp, 0, sizeof( RGBTRIPLE ) * Y );
    memset( temp2, 0, sizeof( RGBTRIPLE ) * Y * X );

    //對每個指標陣列裡的指標宣告一個長度為X的陣列
    for ( int i = 0; i < Y; i++ ) {
        temp[i] = &temp2[i * X];
    }

    return temp;
}
/*********************************************************/
/* 交換二個指標                                          */
/*********************************************************/
void swap( RGBTRIPLE *a, RGBTRIPLE *b ) {
    RGBTRIPLE *temp;
    temp = a;
    a = b;
    b = temp;
}
