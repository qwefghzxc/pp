#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define llSize sizeof(long long)
#define MPILL MPI_LONG_LONG

long long hit;
long long unit;

void random_shoot() {
    srand(time(NULL));
    for (int i = 0; i < unit; ++i) {
        float x = ((rand() % 200000) - 100000) / 100000.0;
        float y = ((rand() % 200000) - 100000) / 100000.0;
        hit += ((x * x + y * y) <= 1) ? 1 : 0;
    }
}

int main() {
    int my_rank, total_ranks;
    long long darts = 0;
    double start_time = 0.0;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &total_ranks);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    // rank 0 input num of darts, and other ranks wait for it
    if (my_rank == 0) {
        // input num of darts
        printf("Total Num of Darts : ");
        scanf("%lld", &darts);
        // partition and send to other process
        unit = darts / total_ranks;
        for (int i = 1; i < total_ranks; ++i)
            MPI_Send(&unit, llSize, MPILL, i, 1, MPI_COMM_WORLD);
        // start counting time after send to other processes
        start_time = MPI_Wtime();
    } else {
        MPI_Status status;
        MPI_Recv(&unit, llSize, MPILL, 0, 1, MPI_COMM_WORLD, &status);
    }

    // go some things
    // printf("This is rank [%d], recv unit is %lld\n", my_rank, unit);
    random_shoot();

    // deal with all results
    if (my_rank != 0) {
        // send result to rank 0
        MPI_Send(&hit, llSize, MPILL, 0, 0, MPI_COMM_WORLD);
    } else {
        // get result from other ranks
        MPI_Status status;
        long long res = 0;
        for (int i = 1; i < total_ranks; ++i) {
            MPI_Recv(&res, llSize, MPILL, i, 0, MPI_COMM_WORLD, &status);
            hit += res;
        }
        // printf all hit and estimate pi
        double cost_time = MPI_Wtime() - start_time;
        double est = 4 * hit / (double)darts;
        printf("%lld/%lld, pi=%lf, cost:%lf s\n", hit, darts, est, cost_time);
    }

    MPI_Finalize();
    return 0;
}
