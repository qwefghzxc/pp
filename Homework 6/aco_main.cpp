#include "ACO.h"

static int getCityNumbers( string &fin );
static int **getPathDistance( string &fin, int num );

int main( int argc, char const *argv[] ) {

    string file( "gr17_d.txt" );

    int cities = getCityNumbers( file );
    int **distance = getPathDistance( file, cities );

    ACO aco( cities, distance );

    // 進行 N 次 ACO
    for ( int i = 1; i < cities; ++i ) {
        DEBUG_nl_raw();
        DEBUG_nl_raw();
        DEBUG_nl_raw();
        DEBUG_nl( ">>>>> ROUND %2d", i );
        aco.NextTurn();
    }

    // 回到原點
    aco.FinishTour();

    aco.GetCurrentCost();
    return 0;
}
// 
//
// 從檔名取出 matrix size (city number)
//
// 
static int getCityNumbers( string &fin ) {
    int size = 0;
    for ( int i = 0; i < fin.length(); ++i ) {

        // 以 _ 作為 delimeter
        if ( fin[i] == '_' )
            break;

        else if ( '0' <= fin[i] && fin[i] <= '9' ) {
            size *= 10;
            size += fin[i] - '0';
        }
        else
            size = 0;
    }

    return size;
}
// 
//
// 檔案內容讀取成 2d array
//
// 
static int **getPathDistance( string &fin, int num ) {

    int **distance = new int *[num];
    for ( int i = 0; i < num; ++i )
        distance[i] = new int[num];

    // 開啟檔案
    FILE *file = fopen( fin.c_str(), "r" );

    // 讀取 num 列，每列 num 個數字
    for ( int i = 0; i < num; ++i ) {
        for ( int j = 0; j < num; ++j )
            fscanf( file, "%d", &distance[i][j] );
    }

    // 關閉檔案並回傳結果
    fclose( file );
    return distance;
}
