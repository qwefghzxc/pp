#include "ACO.h"


//
//
// CTOR, 初始化所有必要資訊
//
//
ACO::ACO( int cities, int **distance ) {
    //
    InitCities( cities, distance );
}
//
//
// 根據 distance 初始化 city
//
//
void ACO::InitCities( int num, int **distance ) {

    this->cities = num;

    // 建立儲存資訊（dis, inv_dis, pheromone）的 2d array
    this->pathInfo = new pPathInfo[num];
    for ( int i = 0; i < num; ++i )
        this->pathInfo[i] = new PathInfo[num];


    // 更新 city 之間的資訊
    for ( int i = 0; i < num; ++i )
        for ( int j = 0; j < num; ++j )
            this->pathInfo[i][j] = PathInfo( distance[i][j], DEFAULT_PHEROMONE );


    // 隨機將 ANTs 分配到任意 city
    srand( time( NULL ) );
    for ( int i = 0; i < ANT_NUM; ++i )
        this->ants[i] = ANT( i, rand() % cities, cities );
}
//
//
// 所有 ant 都走一輪，並在結束時更新 pheromone
//
//
void ACO::NextTurn() {

    for ( int id = 0; id < ANT_NUM; ++id ) {
        Ant &ant = this->ants[id];
        PickNextCity( ant );
    }
    Vaporize();
}
//
//
// 獲取某個 ant 當前 path 的 cost
//
//
double ACO::GetPathCost( Ant &ant ) { return this->pathInfo[ant.currCity][ant.nextCity].distance; }
//
//
// 經過所有 city 後還要回到 init city
//
//
void ACO::FinishTour() {
    // 最終目標： init city
    for ( int i = 0; i < ANT_NUM; ++i )
        this->ants[i].nextCity = this->ants[i].initCity;

    // 計算 Vaporize
    Vaporize();
}
//
//
// 決定該 ant 下一個要去的 city
//
//
void ACO::PickNextCity( Ant &ant ) {

    // 目前最佳 city 以及去該 city 的 cost
    pair<int, double> currentBest( -1, DBL_MIN );


    // ANT k, from i to j 的機率 P^k_ij
    //
    // if : j 是 ANT k 沒經過的 city 的話
    //      Prob = (此路徑 cost)/(所有還沒去的 city 的 cost 總和)
    //
    // else : j 已經經過
    //      Prob = 0
    //
    // cost = pheromone^ALPHA * invDistance^BETA

    // 依序計算前往為拜訪 city 的 path 的 cost
    for ( auto itCity = ant.unvisited.begin(); itCity != ant.unvisited.end(); ++itCity ) {
        const int &target = *itCity;

        // 計算 cost = pheromone^ALPHA * invDistance^BETA
        double cost = pow( pathInfo[ant.currCity][target].pheromone, ALPHA ) *
                      pow( pathInfo[ant.currCity][target].invDistance, BETA );

        // 如果比當前最佳 city 更好就更新
        if ( cost > currentBest.second )
            currentBest = make_pair( target, cost );
    }

    // 最佳 city 移除 visited
    auto itDest = ant.unvisited.find( currentBest.first );
    ant.unvisited.erase( itDest );

    // 更新 target
    ant.nextCity = currentBest.first;

    // DEBUGGING : remain cities
    // DEBUG_nl( "ant[%2d] : %2d -> %2d (%.4lf)", ant.id, ant.currCity, ant.nextCity, GetPathCost( ant ) );
    // for ( auto city : ant.unvisited )
    //     DEBUG_raw( " %2d,", city );
    // DEBUG_nl( "%s", "END" );
}


//
// 每一輪結束後要更新各路徑間的 pheromone
//
void ACO::Vaporize() {

    // 更新各個 ant 路線的 pheromone
    double dPheromone[this->cities][this->cities];
    memset( dPheromone, 0, sizeof( double ) * this->cities * this->cities );

    for ( int id = 0; id < ANT_NUM; ++id ) {

        Ant &ant = this->ants[id];

        // 前往該 city
        ant.cost += GetPathCost( ant );
        // TODO 留下 pheromone
        dPheromone[ant.currCity][ant.nextCity] += pow( GetPathCost( ant ), -1 );
        // 抵達新 city
        ant.currCity = ant.nextCity;
    }

    // 更新 pheromone
    for ( int i = 0; i < this->cities; ++i ) {
        for ( int j = 0; j < this->cities; ++j ) {

            // pheromone 自然散失比例
            double vapoRate = ( 1 - RHO );

            // TODO 更新 pheromone（+散失）
            this->pathInfo[i][j].pheromone = vapoRate * this->pathInfo[i][j].pheromone + dPheromone[i][j];
        }
    }
}
//
//
//
//
//
void ACO::GetCurrentCost() {

    // 找出此輪最短路徑長
    int best = INT32_MAX;
    for ( auto ant : this->ants ) {
        DEBUG_nl( "ANT[%2d] cost : %4d", ant.id, ant.cost );
        best = min( best, ant.cost );
    }

    // 更新最佳路徑
    if ( best == this->currentBestCost )
        DEBUG( "Current Best No Change : %4d", this->currentBestCost );
    else {
        this->currentBestCost = best;
        DEBUG( "Current Best : %4d", this->currentBestCost );
    }
}