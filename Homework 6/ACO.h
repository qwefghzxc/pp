#include <cfloat>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <set>
#include <string>
// #include <mpi.h>
#include <omp.h>

using namespace std;


#define ALPHA 1
#define BETA 1
// 散失比例 (1-RHO)
#define RHO 0.5

#define ANT_NUM 5

#define DEFAULT_PHEROMONE 1
#define DEFAULT_CITY 0


#define DEBUG_raw( format, ... ) printf( format, __VA_ARGS__ )
#define DEBUG( format, ... ) printf( "DEBUG >>> " format, __VA_ARGS__ )
#define DEBUG_nl( format, ... ) printf( "DEBUG >>> " format "\n", __VA_ARGS__ )
#define DEBUG_nl_raw() printf( "\n" )
#define DEBUG_end( format, ... ) printf( "<<< DEBUG : " format "\n------- DEBUG END\n\n", __VA_ARGS__ )

typedef struct ANT {

    int id, cost;

    int initCity, currCity, nextCity;
    set<int> unvisited;

    ANT() {
        this->currCity = -1;
        this->nextCity = -1;
    }

    ANT( int id, int initCity, int cities ) : id( id ), cost( 0 ), initCity( initCity ) {

        // 選擇初始 city 以及建立 unvisited
        this->currCity = initCity;

        for ( int iCity = 0; iCity < cities; ++iCity )
            if ( iCity != initCity )
                this->unvisited.insert( iCity );
    }

} * pAnt, Ant;

typedef struct PATHINFO {
    double distance, invDistance;
    double pheromone;

    PATHINFO() { this->pheromone = DEFAULT_PHEROMONE; }

    PATHINFO( double d, double p ) {
        // assign distance and pheromone
        this->distance = d;
        this->pheromone = p;

        // calc inverse distance
        this->invDistance = pow( d, -1 );
    }

} * pPathInfo, PathInfo;



class ACO {

    int cities = 0;
    Ant ants[ANT_NUM];
    int **traffic;
    PathInfo **pathInfo;
    int currentBestCost = INT32_MAX;


public:
    ACO( int cities, int **distance );
    void TravelAllCities();
    void NextTurn();
    double GetPathCost( Ant &ant );
    void FinishTour();
    void GetCurrentCost();

private:
    void InitCities( int num, int **distance );
    void Vaporize();
    void PickNextCity( Ant &ant );
};
