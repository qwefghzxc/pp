#include <cstdlib>
#include <cstdio>
#include <ctime>


int compare( const void *a, const void *b ) {
    int intA = *(int *)a;
    int intB = *(int *)b;

    if ( intA > intB )
        return 1;
    if ( intA < intB )
        return -1;
    return -1;
}

int main( int argc, char const *argv[] ) {

    int size = 10000;
    int *arr = new int[size];

    // random generate $size numbers
    srand( time( NULL ) );
    for ( int i = 0; i < size; ++i ) {
        arr[i] = rand() % 2147483647;
        arr[i] *= ( rand() % 2 ? 1 : -1 );
    }

    //
    qsort( (void *)arr, size, sizeof( int ), compare );


    // for ( int i = 0; i < size; ++i ) {
    //     for ( int j = 0; j < 10 && i < size; ++i, ++j )
    //         printf( "%d ,", arr[i] );
    //     printf( "\n" );
    // }

    return 0;
}
