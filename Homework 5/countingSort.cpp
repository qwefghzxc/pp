#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <omp.h>

using namespace std;

void CountingSort( int a[], int n ) {
    int i, j, count;

    int *temp = (int *)malloc( n * sizeof( int ) );

// clang-format off
    #pragma omp parallel shared(a, n, temp) private(i, j, count)
    #pragma omp for 
    for ( i = 0; i < n; i++ ) {
        count = 0;
        for ( j = 0; j < n; j++ )
            if ( a[j] < a[i] )
                count++;
            else if ( a[j] == a[i] && j < i )
                count++;

        temp[count] = a[i];
    }
    // clang-format on
    memcpy( a, temp, n * sizeof( int ) );
    free( temp );
}


int main( int argc, char const *argv[] ) {

    int size = 10000;
    int *arr = new int[size];

    // random generate $size numbers
    srand( time( NULL ) );
    for ( int i = 0; i < size; ++i ) {
        arr[i] = rand() % 2147483647;
        arr[i] *= ( rand() % 2 ? 1 : -1 );
    }

    //
    CountingSort( arr, size );

    // for ( int i = 0; i < size; ++i ) {
    //     for ( int j = 0; j < 10 && i < size; ++i, ++j )
    //         printf( "%d ,", arr[i] );
    //     printf( "\n" );
    // }

    return 0;
}
