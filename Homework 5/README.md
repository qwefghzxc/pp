# Compile Command & Execute Command

## Producer Consumer Problem
g++ -std=c++0x -fopenmp -Wall producer_consumer.cpp -o producer_consumer
./producer_consumer ./files/ ./keyword.txt

## Count Sort Problem
g++ -std=c++0x -fopenmp -Wall count_sort.cpp -o count_sort
./count_sort

