#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <map>
#include <omp.h>
#include <queue>
#include <string>
#include <unistd.h>


using namespace std;

static queue<string> sharedMessages;
static vector<string> inputFiles;
static map<string, int> keywords;

static omp_lock_t change_queue_size;

static int iFile;

#define PRODUCERS 1
static int producerDone = 0;
#define CONSUMERS 1
static int consumerDone = 0;

void Producer();
void Consumer();


static void GetFilesInThisDir( string &dir ) {

    // list all files under this dir and redirect to file
    system( ( "ls -1 \"" + dir + "\" > ./inputs.txt" ).c_str() );

    // open inputs.txt and read all files
    fstream inputs( "./inputs.txt", ios_base::in );

    // add all files into input filee queue
    for ( string filename; getline( inputs, filename ); )
        inputFiles.push_back( dir + filename );
}


static void GetKeywordsFrom( string &kwFile ) {
    // file stream of keyword file
    fstream fs( kwFile.c_str(), ios_base::in );

    // input keywords until eof
    string keyword;
    while ( fs >> keyword ) {
        // insert this keyword in map only if not exist
        auto itMap = keywords.find( keyword );
        if ( itMap == keywords.end() )
            keywords.insert( make_pair( keyword, 0 ) );
    }
}


static void KeywordsCountingResult() {

    for ( auto it = keywords.begin(); it != keywords.end(); ++it ) {
        const pair<string, int> &kv = *it;
        printf( "%s : %d\n", kv.first.c_str(), kv.second );
    }

    // for ( auto &kv : keywords )
    //     printf( "%s : %d\n", kv.first.c_str(), kv.second );
}

static void TokenizeWithSpace( string line, vector<string> &tokens ) {

    size_t spacePos;

    while ( ( spacePos = line.find( " " ) ) != string::npos ) {
        // get space pos and token
        tokens.push_back( line.substr( 0, spacePos ) );
        // erase this token
        line.erase( 0, spacePos + 1 );
    }
}


static void LaunchTasks() {
    int thread_id = omp_get_thread_num();

    if ( thread_id < PRODUCERS )
        Producer();
    else
        Consumer();
}



int main( int argc, char const *argv[] ) {

    // get input and output file name
    string inputsDir( argv[1] );
    string keywordsFile( argv[2] );

    // replace space with \\SPACE

    // get all files' name under inputs dir
    auto &files = inputFiles;
    GetFilesInThisDir( inputsDir );

    // open keywords file and read all keywords
    auto &kws = keywords;
    GetKeywordsFrom( keywordsFile );

// clang-format off
    // launch producer and consumer
    #pragma omp shared( keywords, sharedMessages, producerDone, consumerDone )
    #pragma omp parallel num_threads( CONSUMERS + PRODUCERS )
    LaunchTasks();

    // wait for both consumers and producers
    while ( (consumerDone + producerDone) < (CONSUMERS + PRODUCERS) )
        ;
    KeywordsCountingResult();
    // clang-format on
    return 0;
}

//
//

void Producer() {

    auto &tmp = sharedMessages;


// clang-format off
    // open each file seperately
    #pragma omp parallel for num_threads( PRODUCERS )
    // clang-format on
    for ( iFile = 0; iFile < inputFiles.size(); ++iFile ) {
        string file = inputFiles[iFile];

        // open input file stream for this file
        fstream ifs( file.c_str() );

        // get lines from this file
        string line;
        while ( getline( ifs, line ) ) {

            omp_set_lock( &change_queue_size );
            sharedMessages.push( line );
            omp_unset_lock( &change_queue_size );
        }

        // close file stream
        ifs.close();
    }

// clang-format off
    // <atomic> count producerDone
    #pragma omp atomic
    ++producerDone;
    // clang-format on
}

void Consumer() {

    auto &tmp = keywords;

    auto PopFront = []( queue<string> &src ) {
        string line = src.front();
        src.pop();
        return line;
    };

    // count all token from input file until producerDone and share queue isEmpty
    while ( true ) {

        omp_set_lock( &change_queue_size );
        bool isEmpty = sharedMessages.empty();
        omp_unset_lock( &change_queue_size );

        // check if all producer done and share queue is isEmpty
        if ( isEmpty && ( producerDone == PRODUCERS ) )
            break;

        // get a line if not isEmpty
        if ( isEmpty == false ) {

            // <LOCK> get a line from share queue
            omp_set_lock( &change_queue_size );
            string line = PopFront( sharedMessages );
            omp_unset_lock( &change_queue_size );

            // tokenize this line
            vector<string> tokens;
            TokenizeWithSpace( line, tokens );

            // compare all token with keywords
            for ( auto it = tokens.begin(); it != tokens.end(); ++it ) {
                const string &token = *it;

                // check if token in keywords
                auto itFind = keywords.find( token );

                // <atomic> if this token is keyword, keyword count++
                if ( itFind != keywords.end() ) {
                    // clang-format off
                    #pragma omp atomic
                    ++itFind->second;
                    // clang-format on
                }
            }
        }
    }

// clang-format off
    #pragma omp atomic
    ++consumerDone;
    // clang-format on
}
